1. **is_leaf**：Tensor 是否为叶子节点。叶子节点是整个计算图的根基。在前向传播的时候，都是根据叶子节点计算的，在反向传播的过程中也是，所有的梯度计算都要依赖于叶子节点。

   叶子节点是为了节省内存。在反向传播以后，非叶子节点的梯度都会被释放。

2. **requires_grad**：如果需要为 Tensor 计算梯度，requires_grad=True，否则为False。

   使用 pytorch 创建 Tensor 时，可以指定 requires_grad 为 True（默认为 False）。

3. **grad_fn**： 记录变量是怎么来的，方便计算梯度，用于指导反向传播。用来记录变量是怎么来的，方便计算梯度。

   例如：y = x * x。grad_fn 记录了 y 的计算的过程。

    grad_fn 为 None 的 Tensor 都是 leaf variable，反之皆不是。 

4. **grad**：当执行完了 backward 之后，通过 x.grad 查看 x 的梯度值。


[TOC]

# 一、计算机视觉：Transformer

## 2020年

### 1. ViT：Transformer杀入CV界

- 论文： [2010.11929.pdf (arxiv.org)](https://arxiv.org/pdf/2010.11929.pdf) 

-  [[PDF\] An Image is Worth 16x16 Words: Transformers for Image Recognition at Scale | Semantic Scholar](https://www.semanticscholar.org/paper/An-Image-is-Worth-16x16-Words%3A-Transformers-for-at-Dosovitskiy-Beyer/7b15fa1b8d413fbe14ef7a97f651f47f5aff3903) 
- 



## 2021年

### 1. CLIP：图片和文本之间的对比学习

- 

### 2. Swin Transformer：多层次的Vision Transformer

- 论文： [2103.14030.pdf (arxiv.org)](https://arxiv.org/pdf/2103.14030.pdf) 

-  [[PDF\] Swin Transformer: Hierarchical Vision Transformer using Shifted Windows | Semantic Scholar](https://www.semanticscholar.org/paper/Swin-Transformer%3A-Hierarchical-Vision-Transformer-Liu-Lin/c8b25fab5608c3e033d34b4483ec47e68ba109b7) 
- 

### 3. MLP-Mixer：用MLP替换self-attention

- 论文： [2105.01601.pdf (arxiv.org)](https://arxiv.org/pdf/2105.01601.pdf) 
-  [[PDF\] MLP-Mixer: An all-MLP Architecture for Vision | Semantic Scholar](https://www.semanticscholar.org/paper/MLP-Mixer%3A-An-all-MLP-Architecture-for-Vision-Tolstikhin-Houlsby/2def61f556f9a5576ace08911496b7c7e4f970a4) 

### 4. MAE：BERT的CV版

-  论文：[2111.06377.pdf (arxiv.org)](https://arxiv.org/pdf/2111.06377.pdf) 
-  [[PDF\] Masked Autoencoders Are Scalable Vision Learners | Semantic Scholar](https://www.semanticscholar.org/paper/Masked-Autoencoders-Are-Scalable-Vision-Learners-He-Chen/6351ebb4a3287f5f3e1273464b3b91e5df5a16d7) 







# 二、计算机视觉：CNN（2D）

## 2019年

### 1. EfficientNet：通过架构搜索得到的CNN 

- 论文： [EfficientNet: Rethinking Model Scaling for Convolutional Neural Networks (arxiv.org)](https://arxiv.org/pdf/1905.11946.pdf) 
- 

## 2021年

### 1. NON-DEEP NETWORKS ： 让不深的网络也能在ImageNet刷到SOTA 

- 论文：https://arxiv.org/pdf/2110.07641.pdf
- 

# 三、3D 卷积

论文：3D Convolutional Neural Networks for Human Action Recognition 

# 四、视频

## 2016年

- 论文：Unsupervised learning for physical interaction through video prediction（ [1605.07157.pdf (arxiv.org)](https://arxiv.org/pdf/1605.07157.pdf) ）
- Code： [Unsupervised Learning for Physical Interaction through Video Prediction | Papers With Code](https://paperswithcode.com/paper/unsupervised-learning-for-physical) 

## 2020年

论文：Latent Video Transformer

## 2022年

- 论文：UniFormer: Unifying Convolution and Self-attention for Visual Recognition
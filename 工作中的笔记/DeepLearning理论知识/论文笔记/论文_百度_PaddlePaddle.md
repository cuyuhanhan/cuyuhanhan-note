# 一、参考网址

1. 论文： https://arxiv.org/abs/1711.09550 
2. github： [GitHub - longxiang92/Flash-MNIST](https://github.com/longxiang92/Flash-MNIST) 
3. https://www.sohu.com/a/301439857_129720
4. https://www.cnblogs.com/gotopaddle/p/10536507.html
5. -

# 二、Attention Cluster 模型

用于视频分类的 Attention Cluster 模型

模型通过带 Shifting operation 的 **Attention clusters**，处理经过 CNN 模型抽取特征的视频的 RGB、光流、音频等数据，实现视频分类。 

------

## 只利用了 CNN 模型

 **Attention Cluster** 仅利用了 CNN 模型，而没有使用 RNN，主要是基于视频的以下几个特点考虑： 

1. **一段视频的连续帧常常有一定的相似性。**对于分类，可能从整体上关注这些相似的特征就足够了，而没有必要去特意观察它们随着时间的细节变化。

2. **视频帧中的局部特征有时就足够表达出视频的类别。**通过一些局部特征，如牙刷、水池，就能够分辨出『刷牙』这个动作。因此，对于分类问题，关键在于找到帧中的关键的局部特征，而非去找时间上的线索。

3. **在一些视频的分类中，帧的时间顺序对于分类不一定是重要的。**虽然帧顺序被打乱，依然能够看出这属于『撑杆跳』这个类别。

   

------

## 使用 **Attention** 机制的好处

1. Attention 的输出本质上是加权平均，这可以避免一些重复特征造成的冗余。

2. 对于一些局部的关键特征，Attention 能够赋予其更高的权重。这样就能够通过这些关键的特征，提高分类能力。

3. Attention 的输入是任意大小的无序集合。无序这点满足我们上面的观察，而任意大小的输入又能够提高模型的泛化能力。



## 模型结构 ![img](http://5b0988e595225.cdn.sohucs.com/images/20190315/f6bfb4307e53425088cd62fcb99cb375.jpeg) 

**1. 局部特征提取。**通过 CNN 模型抽取视频的特征。

**2. 局部特征集成。**基于 Attention 来获取全局特征。Attention 的输出本质上相当于做了加权平均。

**3. 全局特征分类。**将多个全局特征拼接以后，再通过常规的全连接层和 Softmax 或 Sigmoid 进行最后的单标签或多标签分类。


[TOC]



# 0. 讲解网址：

1. Transformer：[详解Transformer （Attention Is All You Need） - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/48508221) （[The Illustrated Transformer – Jay Alammar – Visualizing machine learning one concept at a time. (jalammar.github.io)](http://jalammar.github.io/illustrated-transformer/) ）
2. Multi-Head Attention：[Multi-Head Attention - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/266448080) 
3. Transformer 详解：https://mp.weixin.qq.com/s?__biz=MzI5MDUyMDIxNA==&mid=2247531914&idx=1&sn=3b8d0b4d3821c64e9051a4d645467995&chksm=ec1c9073db6b1965d69cdd29d40d51b0148121135e0e73030d099f23deb2ff58fa4558507ab8&scene=21#wechat_redirect

# 1. Transformer 详解

## 1.1 为什么采用 Attention

作者采用 Attention 机制的原因是考虑到 RNN（或者LSTM，GRU等）的计算限制为是顺序的，也就是说 RNN 相关算法只能从左向右依次计算或者从右向左依次计算，这种机制带来了两个问题： 

1. 时间片 ![[公式]](https://www.zhihu.com/equation?tex=t) 的计算依赖 ![[公式]](https://www.zhihu.com/equation?tex=t-1) 时刻的计算结果，这样限制了模型的并行能力；

2. 顺序计算的过程中信息会丢失，尽管 LSTM 等门机制的结构一定程度上缓解了长期依赖的问题，但是对于特别长期的依赖现象， LSTM 依旧无能为力。

   

## 1.2 Encoder-Decoder 结构

Transformer的本质上是一个Encoder-Decoder的结构。 如论文中所设置的，编码器由6个编码block组成，同样解码器是6个解码block组成。 

 ![img](https://pic3.zhimg.com/80/v2-c14a98dbcb1a7f6f2d18cf9a1f591be6_1440w.jpg) 

Decoder的结构如图5所示，它和encoder的不同之处在于Decoder多了一个Encoder-Decoder Attention，两个Attention分别用于计算输入和输出的权值：

1. Self-Attention：当前翻译和已经翻译的前文之间的关系；
2. Encoder-Decnoder Attention：当前翻译和编码的特征向量之间的关系。

 ![img](https://pic1.zhimg.com/80/v2-d5777da2a84e120846c825ff9ca95a68_1440w.jpg) 



## 1.3 Self-Attention

先看一下Attention的计算方法，整个过程可以分成7步：

1. 将输入单词转化成嵌入向量。
2. 根据嵌入向量得到 ![[公式]](https://www.zhihu.com/equation?tex=q) ， ![[公式]](https://www.zhihu.com/equation?tex=k) ， ![[公式]](https://www.zhihu.com/equation?tex=v) 三个向量。
3. 为每个向量计算一个 score： ![[公式]](https://www.zhihu.com/equation?tex=%5Ctext%7Bscore%7D+%3D+q+%5Ccdot+k) 。
4. 为了梯度的稳定，Transformer 使用了 score 归一化，即除以 ![[公式]](https://www.zhihu.com/equation?tex=%5Csqrt%7Bd_k%7D) 。  d 是 q 跟 k 的维度。因为 score 的数值会随着 dimension 的增大而增大，所以要除以![[公式]](https://www.zhihu.com/equation?tex=%5Csqrt%7Bd_k%7D)的值，相当于归一化的效果。 
5. 对 score 施以 softmax 激活函数。
6. softmax 点乘 Value 值 ![[公式]](https://www.zhihu.com/equation?tex=v) ，得到加权的每个输入向量的评分 ![[公式]](https://www.zhihu.com/equation?tex=v) 。
7. 相加之后得到最终的输出结果 ![[公式]](https://www.zhihu.com/equation?tex=z) ： ![[公式]](https://www.zhihu.com/equation?tex=z%3D%5Csum+v) 。



在self-attention需要强调的最后一点是其采用了[残差网络](https://zhuanlan.zhihu.com/p/42706477) [5]中的short-cut结构，目的当然是解决深度学习中的退化问题。

 ![img](https://pic1.zhimg.com/80/v2-2f06746893477aec8af0c9c3ca1c6c14_1440w.jpg) 

## 1.4 Multi-Head Attention

## 1.5 Batch Norm & Layer Norm

 ![img](https://mmbiz.qpic.cn/sz_mmbiz_jpg/gYUsOT36vfqZuR6BRxTDDm1ic4xiaPIJ1YegSHckQXwWftZYNsr0JLYEXAicOTv1TSchl4G3gbFCy9XyHDX7X95rg/640?wx_fmt=jpeg&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1) 

Batch Normalization 强行让一个batch的数据的某个 channel 的<img src="C:\Users\32918\AppData\Roaming\Typora\typora-user-images\1643268141728.png" alt="1643268141728" style="zoom:50%;" />，而 Layer Normalization 让一个数据的所有 channel 的<img src="C:\Users\32918\AppData\Roaming\Typora\typora-user-images\1643268170828.png" alt="1643268170828" style="zoom:50%;" />。 

# 总结

## **优点**

1. 虽然Transformer最终也没有逃脱传统学习的套路，Transformer也只是一个全连接（或者是一维卷积）加Attention的结合体。但是其设计已经足够有创新，因为其抛弃了在NLP中最根本的RNN或者CNN并且取得了非常不错的效果，算法的设计非常精彩，值得每个深度学习的相关人员仔细研究和品位。
2. Transformer的设计最大的带来性能提升的关键是将任意两个单词的距离是1，这对解决NLP中棘手的长期依赖问题是非常有效的。
3. Transformer不仅仅可以应用在NLP的机器翻译领域，甚至可以不局限于NLP领域，是非常有科研潜力的一个方向。
4. 算法的并行性非常好，符合目前的硬件（主要指GPU）环境。

## **缺点**

1. 粗暴的抛弃RNN和CNN虽然非常炫技，但是它也使模型丧失了捕捉局部特征的能力，RNN + CNN + Transformer的结合可能会带来更好的效果。
2. Transformer失去的位置信息其实在NLP中非常重要，而论文中在特征向量中加入Position Embedding也只是一个权宜之计，并没有改变Transformer结构上的固有缺陷。
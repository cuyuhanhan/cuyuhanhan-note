[TOC]

 **OMNIVORE: A Single Model for Many Visual Modalities** 

we propose a single model which excels at classifying images, videos, and single-view 3D data using exactly the same model parameters. 

we propose a single model which excels at classifying images, videos, and single-view 3D data using exactly the same model parameters. 

 We argue that the first step towards a truly all-purpose vision system is to build models that work seamlessly across modalities, instead of being over-optimized for each modality  

视觉中的ConvNet架构。卷积网络架构[26,46]已经在图像、视频和3D识别中的许多计算机视觉任务中流行起来。2D卷积是卷积网络中用于图像的主要构件[34,44,75,80]，而3D卷积用于3D数据[18,32]，或者与2D卷积结合用于视频识别[13,86,87]。I3D[13]引入了一种将2D图像卷积扩展为3D卷积的方法，它允许视频和3D数据的3D ConvNets通过从预先训练的图像模型初始化来间接利用图像数据。由于视频和3D数据集相对较小，它们受益于膨胀的预训练图像网络。然而，膨胀技术仅适用于模型微调，OMNIVORE模型是在图像、视频和单视图3D数据上联合进行预训练的。

# 1. 一句话总结

基于 transformer ，对不同模式的分类任务进行联合训练，实现了跨模态识别。

# 2. 要解决的问题

 一个模态无关的模型（a modality-agnostic model）可以执行跨模态概化：它可以使用从一个模态学到的东西来执行其他模态的识别。例如，即使它只看到过有标签的南瓜视频，它也可以识别3D图像中的南瓜。

学习一种可以在三种主要的视觉模式下操作的单一模型：图像、视频和单视图3D。 

# 

# 4. 方法

 Our OMNIVORE model does not use a custom architecture for each visual modality.

# 6. 相关工作

视觉中的ConvNet架构。卷积网络架构[26,46]已经在图像、视频和3D识别中的许多计算机视觉任务中流行起来。

2D卷积是卷积网络中用于图像的主要构件[34,44,75,80]。

3D卷积用于3D数据[18,32]，或者与2D卷积结合用于视频识别[13,86,87]。I3D[13]引入了一种将2D图像卷积扩展为3D卷积的方法，它允许视频和3D数据的3D ConvNets通过从预先训练的图像模型初始化来间接利用图像数据。由于视频和3D数据集相对较小，它们受益于膨胀的预训练图像网络。然而，膨胀技术仅适用于模型微调，OMNIVORE模型是在图像、视频和单视图3D数据上联合进行预训练的。


















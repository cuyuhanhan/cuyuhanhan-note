[TOC]



# 1. 查看对应版本

**相关网站：**

- PyTorch官网： [Previous PyTorch Versions | PyTorch](https://pytorch.org/get-started/previous-versions/) （https://pytorch.org/get-started/previous-versions/）

# 2. 安装

## 2.1 pip直接安装

​				pip install torchvision==0.5.0 -f https://download.pytorch.org/whl/torch_stable.html

​				pip install torch==1.4.0 -f https://download.pytorch.org/whl/torch_stable.html

## 2.2 下载后安装

- 查看对应版本。下载网址：https://download.pytorch.org/whl/
- wget https://download.pytorch.org/whl/cu111/torch-1.8.0%2Bcu111-cp36-cp36m-linux_x86_64.whl
- pip install torch-1.8.0%2Bcu111-cp36-cp36m-linux_x86_64.whl

# 3. 验证

## 3.1 查看版本

- import torch
- print(torch.__version__)

## 3.2 python验证

- import torch
- torch.cuda.is_available()  # 看看是不是true
- a = torch.tensor(1)
- a.cuda()  # 看看tensor能不能转移到显卡上
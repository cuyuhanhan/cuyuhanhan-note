**参考网址：**

[(11条消息) AttributeError：module ‘word2vec-google-news-300’ has no attribute ‘load data’_froot的博客-CSDN博客](https://blog.csdn.net/qq_43348528/article/details/114932330)



word2vec-google-news-300.gz 存放在我的百度网盘中。

# 方法一：

1. 手动下载下来 word2vec-google-news-300.gz，对该文件解压缩，得到GoogleNews-vectors-negative300.bin

2. 使用以下代码：

3. - from gensim.models import  KeyedVectors
   - from gensim.test.utils  import datapath
   - wv_from_bin =  KeyedVectors.load_word2vec_format(datapath(r"你本机GoogleNews-vectors-negative300.bin的路径"), binary=True)

# 方法二：

- import gensim
- word2vec = gensim.models.KeyedVectors.load_word2vec_format("GoogleNews-vectors-negative300.bin.gz",     binary=True)



